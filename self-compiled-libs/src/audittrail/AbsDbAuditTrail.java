package com.fmse.abs.orm.audittrail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.text.MessageFormat;

public class AbsDbAuditTrail {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader br = null;
        BufferedWriter bw = null;
        Map<String, String> queries = new HashMap<>();

        try {
            String currentLine;
            String currentTableName = "";
            String currentQuery = "";

            br = new BufferedReader(new FileReader(args[0]));

            while ((currentLine = br.readLine()) != null) {
                if (currentLine.trim().startsWith("CREATE TABLE ")) {
                    currentLine = currentLine.trim();
                    if (!currentTableName.isEmpty() && !currentQuery.isEmpty()) {
                        queries.put(currentTableName, currentQuery);
                    }
                    currentLine = currentLine.replace("  ", " ");
                    currentTableName = currentLine.split(" ")[2];
                    currentQuery = currentLine + "\n";
                } else if (!currentLine.trim().isEmpty()) {
                    currentLine = Pattern.compile("^\\s*id\\s+SERIAL\\s+PRIMARY\\s+KEY", Pattern.CASE_INSENSITIVE)
                        .matcher(currentLine).replaceAll("id INTEGER NOT NULL");
                    currentQuery = currentQuery.concat(currentLine + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error at AbsDbAuditTrail@main");
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("error at AbsDbAuditTrail@main");
            }
        }

        String createTableHead = "CREATE TABLE IF NOT EXISTS {0}Audit (\n" +
            "stamp			TIMESTAMP 	not null,\n" +
            "type 			VARCHAR(6)		not null,\n" +
            "userid 			TEXT		not null,\n";

        String triggerScript = "CREATE OR REPLACE FUNCTION Process{0}Audit() RETURNS TRIGGER AS ${0}Audit$\n" +
            "    begin\n" +
            "        if (TG_OP = ''INSERT'') then\n" +
            "            INSERT INTO {0}Audit SELECT now(), ''INSERT'', user, NEW.*;\n" +
            "        elsif (TG_OP = ''UPDATE'') then\n" +
            "            INSERT INTO {0}Audit SELECT now(), ''UPDATE'', user, NEW.*;\n" +
            "            return NEW;\n" +
            "        elsif (TG_OP = ''DELETE'') then\n" +
            "            INSERT INTO {0}Audit SELECT now(), ''DELETE'', user, OLD.*;\n" +
            "            return NEW;\n" +
            "        end if;\n" +
            "       return null;\n" +
            "    end;\n" +
            "${0}Audit$ LANGUAGE plpgsql;\n\n" +
            "CREATE TRIGGER {0}Audit\n" +
            "AFTER INSERT OR UPDATE OR DELETE ON {0}\n" +
            "    FOR EACH ROW EXECUTE PROCEDURE Process{0}Audit();\n";

        try {
            bw = new BufferedWriter(new FileWriter(args[0], true));

            for (String tableName:queries.keySet()) {
                String currentTriggerScript = MessageFormat.format(triggerScript, tableName) + "\n";
                String currentCreateTableHead = MessageFormat.format(createTableHead, tableName);
                String[] createTableQueryLst = queries.get(tableName).split("\n");
                String createTableQuery = String.join("\n", Arrays.copyOfRange(createTableQueryLst, 1, createTableQueryLst.length)) + "\n";
                String fullAuditQuery = currentCreateTableHead + createTableQuery + "\n" + currentTriggerScript;
                bw.write(fullAuditQuery + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error at AbsDbAuditTrail@main");
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("error at AbsDbAuditTrail@main");
            }
        }
    }
}
