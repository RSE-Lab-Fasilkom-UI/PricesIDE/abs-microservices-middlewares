# Middlewares for ABS Microservices Framework (abs-microservices-middlewares)

Reliable Software Engineering Lab - Faculty of Computer Science - Universitas Indonesia

-----------

## Brief Description
This project contains two subdirectories: `middleware` and `self-compiled-libs`.
`middleware` is a NetBeans project that hosts Middleware module (`middleware.jar`).
`self-compiled-libs` is a collection of self-compiled libraries that acts as additional dependencies/modules in ABS Microservices.

## Prerequisites to Develop
- Java Development Kit (JDK) 8 or later
- Apache Ant

## Middleware module
Middleware module (as written on 08/12/2020) contains:
- `ABSServlet` as webserver routine for ABS Microservices.
- `Authorization` mechanism with OAuth and standard password-based authentication support.
- `ABSCaller` and `Controller` as routing mechanism for ABS Microservices.
- `DataTransformer` as ABS to Java/JSON (vice versa) data converter.

### How to Compile
1. Go to `middleware` subdirectory.
2. If you are compiling for the first time, go to `nbproject/private` (or create if not exist).
   Then create new file called `private.properties` that contains:
   ```java
   javac.target=1.8
   ```
3. Run `ant jar`.
4. The compiled `middleware.jar` will be available on `dist` directory.
   To update your ABS Microservices installation with your recently compiled Middleware, just copy the `dist/middleware.jar` file into ABS Microservices `lib` directory.

## Self-compiled Libraries
Self-compiled libraries (as written on 08/12/2020) contains:
- `AbsDbOrm` as RDBMS ORM for ABS Microservices. Currently only supports PostgreSQL.
  **Source code is located in `src/orm`.**
- `AbsDbAuditTrail` as Audit Trail (database change log) schema generator for ABS Microservices.
  **Source code is located in `src/audittrail`.**

### How to Compile
1. Go to `self-compiled-libs` subdirectory.
2. Run `ant build.[library-name]`.
   For example, if you want to build the Audit Trail library (source is in `src/audittrail`), run `ant build.audittrail`.
4. The compiled JAR file will be available on `dist` directory.
   To update your ABS Microservices installation with your recently compiled library, just copy the corresponding file into ABS Microservices `lib` directory.
