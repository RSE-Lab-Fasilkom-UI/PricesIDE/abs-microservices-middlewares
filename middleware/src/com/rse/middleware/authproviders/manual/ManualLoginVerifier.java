/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rse.middleware.authproviders.manual;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.rse.middleware.authproviders.JWTPayloadAdapter;
import com.rse.middleware.authproviders.TokenPayload;
import com.rse.middleware.authproviders.TokenVerifier;

/**
 *
 * @author ichla
 */
public class ManualLoginVerifier implements TokenVerifier {
    @Override
    public TokenPayload verify(String clientId, String token) {
        try {
            DecodedJWT jwt = JWTUtils.decodeJWT(token.trim());
            TokenPayload payload = null;
            if (jwt != null) {
                payload = new JWTPayloadAdapter(jwt);
                System.out.println(payload.getAudiences());
                System.out.println(payload.getIssuer());
                System.out.println(payload.getEmail());
                if (!payload.getIssuer().equals(JWTUtils.getIssuer())) {
                    throw new IllegalArgumentException("Client ID mismatch");
                }
            } else {
                throw new IllegalArgumentException("Invalid ID token.");
            }
            return payload;
        } catch (IllegalArgumentException e) {
            System.out.println("Token problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return null;
    }
}
