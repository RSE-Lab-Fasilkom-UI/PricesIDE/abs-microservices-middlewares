/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rse.middleware.authproviders.manual;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author ichla
 */
public class JWTUtils {
    private static String getAuthProperty(String key) {
        FileInputStream input = null;
        Properties prop = new Properties();
        try {
            input = new FileInputStream("auth.properties");
            // load a properties file
            prop.load(input);
            return prop.getProperty(key);
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    public static String createJWT(String userId, String email, long ttlMillis) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        String jwtString = null;
        try {
            Algorithm algorithm = Algorithm.HMAC256(getSecretKey());
            JWTCreator.Builder builder = JWT.create()
                    .withIssuer(getIssuer())
                    .withIssuedAt(now)
                    .withSubject(userId + "::" + email)
                    .withClaim("email", email);
            if (ttlMillis > 0) {
                long expMillis = nowMillis + ttlMillis;
                Date exp = new Date(expMillis);
                builder.withExpiresAt(exp);
            }
            jwtString = builder.sign(algorithm);
        } catch (JWTCreationException e){
            System.out.println("Token problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return jwtString;
    }

    public static DecodedJWT decodeJWT(String token) {
        DecodedJWT jwt = null;
        try {
            Algorithm algorithm = Algorithm.HMAC256(getAuthProperty("jwt.secretkey"));
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(getAuthProperty("jwt.issuer"))
                    .build();
            jwt = verifier.verify(token);
        } catch (JWTVerificationException e){
            System.out.println("Token problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return jwt;
    }

    private static String getSecretKey() {
        return getAuthProperty("jwt.secretkey");
    }

    public static String getIssuer() {
        return getAuthProperty("jwt.issuer");
    }
}
