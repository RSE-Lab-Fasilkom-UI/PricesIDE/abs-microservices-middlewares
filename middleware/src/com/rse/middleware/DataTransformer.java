package com.rse.middleware;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.ArrayList;
import ABS.StdLib.Pair;
import ABS.StdLib.Pair_Pair;

import abs.backend.java.lib.runtime.ABSObject;
import abs.backend.java.lib.types.ABSBool;
import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSValue;
import abs.backend.java.lib.types.ABSBuiltInDataType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
 * ACKNOWLADGE: https://github.com/sir-muamua/ABSServer/blob/master/src/com/fmse/absserver/helper/DataTransformer.java
 */
public class DataTransformer {
    public static String convertAbsResponseToJSON(ABSValue obj)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        // Appending Data Key, as requirement of IFML
        return "{\"data\": ".concat(convertAbsDataToJSON(obj)).concat("}");
    }

    public static String convertAbsDataToJSON(ABSValue obj)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (obj == null) {
            return "null";
        } else if (obj instanceof ABS.StdLib.List_Nil) {
            return "[]";
        } else if (obj instanceof ABS.StdLib.List) {
            return convertAbsListToJSON((ABS.StdLib.List) obj);
        } else if (obj instanceof ABSBuiltInDataType) {
            return convertAbsBuiltInDataToJSON(obj);
        } else if (obj instanceof ABS.StdLib.Map){
            return convertAbsMapToJSON((ABS.StdLib.Map<ABSValue, ABSValue>) obj);
        } else {
            return convertAbsObjectToJSON(obj);
        }
    }

    public static String convertAbsBuiltInDataToJSON(ABSValue obj) {
        String value = obj.toString();
        if (obj instanceof ABSString) {
            String tempValue = value.substring(1, value.length() - 1);
            tempValue = tempValue.replace("\\", "\\\\").replace("\"", "\\\"");
            tempValue = tempValue.replaceAll("(\r\n|\n|\r)", "\\\\n");
            return "\"".concat(tempValue).concat("\"");
        }
        return value;
    }

    public static String convertABSStringToJavaString(ABSString target) throws Exception {
        return target.toString().replaceAll("\"", "");
    }

    public static String convertAbsListToJSON(ABS.StdLib.List<ABSValue> dataModels)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String json = "";
        do {
            json = json.concat(convertAbsDataToJSON(ABS.StdLib.head_f.apply(dataModels)));
            dataModels = ABS.StdLib.tail_f.apply(dataModels);
            if (!(dataModels instanceof ABS.StdLib.List_Nil)) {
                json = json.concat(", ");
            }
        } while (!(dataModels instanceof ABS.StdLib.List_Nil));
        return "[".concat(json).concat("]");
    }

    public static String convertAbsObjectToJSON(ABSValue obj)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        ABSObject absobj = (ABSObject) obj;
        Class cls = absobj.getClass();
        List<String> fields = (List<String>) cls.getDeclaredMethod("getFieldNames").invoke(obj);
        if (fields.isEmpty()) {
            return "null";
        }
        int ii = 0;
        String json = "";
        for (String field : fields) {
            json = json.concat("\"" + field + "\"" + ": ");
            Method m = cls.getDeclaredMethod("getFieldValue", field.getClass());
            m.setAccessible(true);
            json = json.concat(convertAbsDataToJSON((ABSValue) m.invoke(obj, field)));
            ii++;
            if (ii < fields.size()) {
                json = json.concat(", ");
            }
        }
        return "{".concat(json).concat("}");
    }

    public static String convertAbsMapToJSON(ABS.StdLib.Map<ABSValue, ABSValue> dataModels) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String json = "";
        ABS.StdLib.Set<ABSValue> keys = ABS.StdLib.keys_f.apply(dataModels);

        do {
            ABSValue keyAbs = ABS.StdLib.take_f.apply(keys);
            String key = convertAbsDataToJSON(keyAbs);
            String value = convertAbsDataToJSON(ABS.StdLib.lookupUnsafe_f.apply(dataModels, keyAbs));

            json = json.concat(key);
            json = json.concat(":");
            json = json.concat(value);

            keys = ABS.StdLib.remove_f.apply(keys, keyAbs);

            if (!ABS.StdLib.emptySet_f.apply(keys).toBoolean()) {
                json = json.concat(", ");
            }
        } while (!ABS.StdLib.emptySet_f.apply(keys).toBoolean());

        return "{".concat(json).concat("}");
    }

    public static ArrayList<String> convertABSListToJavaList(ABS.StdLib.List<ABSString> payload) {
        ArrayList<String> result = new ArrayList<>();
        if (payload.isCons()) {
            do {
                result.add(ABS.StdLib.head_f.apply(payload).getString());
                payload = ABS.StdLib.tail_f.apply(payload);
            } while (!(payload instanceof ABS.StdLib.List_Nil));
        }
        return result;
    }

    public static ABS.StdLib.List<ABSString> convertJSONToABSList(String json) throws JSONException {
        ABS.StdLib.List<ABSString> result = new ABS.StdLib.List_Nil<>();
        JSONArray arr = new JSONArray(json);
        for (int i = 0; i < arr.length(); i++) {
            result = new ABS.StdLib.List_Cons<ABSString>(ABSString.fromString(arr.getString(i)), result);
        }
        return result;
    }

    public static HashMap<String, String> convertABSMapToJavaMap(ABS.StdLib.Map<ABSString, ABSString> payload){
    	HashMap<String, String> result = new HashMap<String, String>();
        ABS.StdLib.Set<ABSString> keys = ABS.StdLib.keys_f.apply(payload);

        do {
            ABSString keyAbs = (ABSString) ABS.StdLib.take_f.apply(keys);
            String key = keyAbs.getString();
            String value = ((ABSString) ABS.StdLib.lookupUnsafe_f.apply(payload, keyAbs)).getString();

            result.put(key, value);
            keys = ABS.StdLib.remove_f.apply(keys, keyAbs);
        } while (!ABS.StdLib.emptySet_f.apply(keys).toBoolean());

        return result;
    }

    public static ABS.StdLib.Map<ABSString, ABSString> convertJSONToABSMap(String json) throws JSONException{
        // STR: convert output
        ABS.StdLib.Map<ABSString, ABSString> result = new ABS.StdLib.Map_EmptyMap<ABSString, ABSString>();

        // convert to json object
        JSONObject jsonObject =  new JSONObject(json);
        JSONArray keys = jsonObject.names();

        // convert JSON to map
        for (int ii = 0; ii < keys.length(); ii++) {
            String paramName = keys.getString(ii);
            String paramValue = jsonObject.getString(paramName);
            System.out.println("key: "+paramName+", value: "+paramValue);

            ABSString key = ABSString.fromString(paramName);
            ABSString value = ABSString.fromString(paramValue.toString());
            result = DataTransformer.addPair(result, key, value);
        }
        return result;
    }



    private static ABS.StdLib.Map_InsertAssoc<ABSString, ABSString> addPair(ABS.StdLib.Map<ABSString, ABSString> absRequestInputMap, ABSString key, ABSString value) {
        Pair<ABSString, ABSString> methodPair = new Pair_Pair<ABSString, ABSString>(key, value);
        return new ABS.StdLib.Map_InsertAssoc<ABSString, ABSString>(methodPair, absRequestInputMap);
    }

}
